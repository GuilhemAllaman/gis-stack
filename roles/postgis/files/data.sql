-- dummy data
insert into geo_point(name, geom)
values
  ('p1', ST_SetSRID(ST_MakePoint(1, 1), 4326)),
  ('p2', ST_SetSRID(ST_MakePoint(2, 2), 4326)),
  ('p3', ST_SetSRID(ST_MakePoint(3, 3), 4326))
;
