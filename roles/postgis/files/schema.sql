-- dummy geo table with point geometry
create table geo_point (
  id serial,
  name text not null,
  geom geometry(point, 4326) not null
);
create index geo_point_namde_index on geo_point(name);
create index geo_point_geom_index on geo_point using gist(geom);

-- dummy geo table with linestring geometry
create table geo_line (
  id serial,
  name text not null,
  geom geometry(linestring, 4326) not null
);
create index geo_line_namde_index on geo_line(name);
create index geo_line_geom_index on geo_line using gist(geom);

-- dummy geo table with polygon geometry
create table geo_polygon (
  id serial,
  name text not null,
  geom geometry(polygon, 4326) not null
);
create index geo_polygon_namde_index on geo_polygon(name);
create index geo_polygon_geom_index on geo_polygon using gist(geom);
