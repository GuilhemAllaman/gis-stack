# GIS stack setup


### Required dependencies


```bash
apt install ansible
```


### Host declaration


When creating a new host, it is recommended to also create a group and put this host inside, so that a secure and encrypted vault can be used to save passwords.
Dont't forget also to set host's `ansible_user` in `hosts` file.

Servers (geoserver, mapserver, qgisserver, openmaptiles) can be disabled with `xxx_enabled: no` vars.

Ports, login, mails, passwords ...etc should be specified in each host


### Infos about a host's variables


```bash
export HOST=guilhemallaman.net
ansible-inventory -i env --host $HOST --vars
```


### Deploy GIS stack


Reminders:
- ansible's option `-k` is for specifying ssh password (if kays are OK, this option is not needed)
- ansible's option `-K` is for specifying sudo password, so it will almost always be necessary


##### Deploy `setup` playbook

First run playbook with `setup` tag: it will create basic stuff needed after, such as ssl certificates, docker-compose file ...

```bash
ansible-playbook deploy.yaml -i env -l $HOST -t setup -K --ask-vault-pass # --vault-password-file env/group_vars/$GROUP/.vault_pwd
```


##### Create DNS records for related subdomain

Those DNS records can be CNAME: they should all point to the same host


##### Retrieve letsencrypt ssl certificates

A bash script has been generated, that will get some ssl certificates for the geoservices using letsencrypt's certbot.
The script file should be localed at `/var/gis/getcerts.sh` and can be run


##### Deploy geoservices playbook

Depending on the geoservices you want to setup, run the playbook with specific tags related to the geoservices you want to setup:

```bash
ansible-playbook deploy.yaml -i env -l $HOST -t [postgis,pgadmin,geoserver,mapserver,qgisserver,openmaptiles] -K --ask-vault-pass # --vault-password-file env/group_vars/$GROUP/.vault_pwd
```

example for ga.net:
```bash
ansible-playbook deploy.yaml -i env -l guilhemallaman.net -t setup,postgis,pgadmin,geoserver,mapserver,qgisserver,openmaptiles -K --vault-password-file env/group_vars/ga/.vault_pwd
```

### GeoServer setup

- login to geoserver using http and redirect port: for this you maybe need to edit `docker-compose.yaml` file and change geoserver's bind address to `0.0.0.0`
- do following steps in the admin web interface:
  - change admin's password
  - go to `Global Settings` on the left, set the `Proxy Base URL` with your domain (`https://your.geoserver.domain`) and check the `Use headers for Proxy URL ` box
- restart geoserver with docker-compose
